import { Option } from "./Option";

type Kind = "Ok" | "Err";

/**
 * Result type to handle errors. Inspired by the rust result enum.
 * Can take two forms: ok-result and err-result.
 * ok-result represents a value. err-result represents an error
 */
export class Result<T, E> {
  private readonly kind: Kind;
  private readonly val?: T;
  private readonly err?: E;
  private constructor({ kind, val, err }: { kind: Kind, val?: T, err?: E }) {
    this.kind = kind;
    this.val = val;
    this.err = err;
    Object.freeze(this);
  }

  /**
   * Indicates if this is a ok-result
   */
  get isOk(): boolean {
    return this.kind === "Ok";
  }

  /**
   * Indicates if this is a err-result
   */
  get isErr(): boolean {
    return this.kind === "Err";
  }

  /**
   * Pattern-matches the inner value. 'onOk' is executed when the inner value is defined and 'onErr' when this contains an error.
   * If both given closures return a value of the same type, then a value of this type is returned by the exectued branch.
   * @param branches closures which are used to operate on the different branches the Result can take
   * @returns the value returned by the executed branch
   */
  match<R>({ onOk, onErr }: { onOk: (ok: T) => R, onErr: (err: E) => R }): R {
    if (this.isOk) return onOk(this.val!);
    else return onErr(this.err!);
  }

  /**
   * Executes the given closure when this is ok-result
   * @param call invoked when this is ok-result
   */
  ifOk<R>(call: (ok: T) => R): Option<R> {
    if (this.isOk) return Option.some(call(this.val!));
    else return Option.none();
  }

  /**
   * Executes the given closure when this is err-result
   * @param call invoked when this is err-result
   */
  ifErr<R>(call: (err: E) => R): Option<R> {
    if (this.isErr) return Option.some(call(this.err!));
    else return Option.none();
  }

  /**
   * Gets the inner ok-value if present. Returns the fallback value otherwise
   * @param fallback value to return if this is an err-result
   * @returns inner ok-value or fallback-value
   */
  getOr(fallback: T | ((err: E) => T)): T {
    function isFunction(func: T | ((err: E) => T)): func is (err: E) => T {
      return typeof func === 'function';
    }
    return this.match({
      onOk: (some) => some,
      onErr: (err) => isFunction(fallback)
        ? fallback(err)
        : fallback,
    })
  }

  /**
   * Factory method to produce an ok-result with the given value
   * @param val value contained by the produced ok-result
   * @returns Result<T> with the given value inside
   */
  static ok<T, E>(val: T): Result<T, E> {
    return new Result({ kind: "Ok", val });
  }

  /**
   * Factory method to produce a err-result, which represents an undefined value
   * @returns a err-result
   */
  static err<T, E>(err: E): Result<T, E> {
    return new Result({ kind: "Err", err });
  }
}
