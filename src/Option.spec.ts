import { Option } from './Option';

describe('Option', () => {
  it('should be immutable', () => {
    const someNumber = Option.some(1);
    const nothing = Option.none();
    expect(() => { (someNumber as any).val = 2; }).toThrow();
    expect(() => { (nothing as any).val = 2; }).toThrow();
  });

  describe('some()', () => {
    it('should produce a Option with a value inside', () => {
      const someNumber = Option.some(1);
      expect((someNumber as any).val).toBe(1);
    });
  });

  describe('none()', () => {
    it('should produce a Option with no value inside', () => {
      const nothing = Option.none();
      expect((nothing as any).val).toBeUndefined();
    });
  });

  describe('from()', () => {
    it('should produce an Option.some() when given a non-null value', () => {
      const nullableNumber: Number | null | undefined = 1;
      expect((Option.from(nullableNumber) as any).val).toBe(1);
    });

    it('should produce an Option.none() when given a null value', () => {
      const nullableNumber: Number | null = null;
      let undefinableNumber: Number | undefined;
      expect((Option.from(nullableNumber) as any).val).toBeUndefined();
      expect((Option.from(undefinableNumber) as any).val).toBeUndefined();
    });
  })

  describe('match()', () => {
    it('should execute the onSome function when created with Option.some()', () => {
      const someNumber = Option.some(1);
      let didOnSome = false;
      let didOnNone = false;
      someNumber.match({
        onSome: () => { didOnSome = true; },
        onNone: () => { didOnNone = true; }
      });
      expect(didOnSome).toBe(true);
      expect(didOnNone).toBe(false);
    });

    it('should execute the onNone function when created with Option.none()', () => {
      const nothing = Option.none();
      let didOnSome = false;
      let didOnNone = false;
      nothing.match({
        onSome: () => { didOnSome = true; },
        onNone: () => { didOnNone = true; }
      });
      expect(didOnSome).toBe(false);
      expect(didOnNone).toBe(true);
    });

    it('should return a value when onSome and onNone both return values', () => {
      const someNumber = Option.some(1);
      const onSome = () => 1;
      const onNone = () => 0;

      const onSomeResult = someNumber.match({ onSome, onNone });

      const nothing = Option.none();
      const onNoneResult = nothing.match({ onSome, onNone });

      expect(onSomeResult).toBe(1);
      expect(onNoneResult).toBe(0);
    });

    describe('{ onSome }', () => {
      it('should receive the inner value', () => {
        const someNumber = Option.some(1);
        let innerValue = 0;
        someNumber.match({
          onSome: (some) => { innerValue = some; },
          onNone: () => { /* no-op */ }
        });
        expect(innerValue).toBe(1);
      });
    });
  });

  describe('ifSome()', () => {
    it('should run the given function only when the object was created with Option.some()', () => {
      const someNumber = Option.some(1);
      let wasExecuted = false;

      someNumber.ifSome(() => {
        wasExecuted = true;
      });

      expect(wasExecuted).toBe(true);
    });

    it('should not run the given function when the object was Option.none()', () => {
      const nothing = Option.none();
      let wasExecuted = false;

      nothing.ifSome(() => {
        wasExecuted = true;
      });

      expect(wasExecuted).toBe(false);
    });

    it('should return an Option<R> when called', () => {
      const numOption = Option.some(1);

      const numberOption = numOption.ifSome(() => {
        return 2;
      });

      expect((numberOption as any).val).toBe(2);
    });
  });

  describe('ifNone()', () => {
    it('should run the given function only when the object was created with Option.none()', () => {
      const nothing = Option.none();
      let wasExecuted = false;

      nothing.ifNone(() => {
        wasExecuted = true;
      });

      expect(wasExecuted).toBe(true);
    });

    it('should not run the given function when the object was Option.some()', () => {
      const someNumber = Option.some(1);
      let wasExecuted = false;

      someNumber.ifNone(() => {
        wasExecuted = true;
      });

      expect(wasExecuted).toBe(false);
    });

    it('should return an Option<R> when called', () => {
      const nothing = Option.none();

      const numberOption = nothing.ifNone(() => {
        return 2;
      });

      expect((numberOption as any).val).toBe(2);
    });
  });

  describe('getOr()', () => {
    it('should return the some-value if one is present', () => {
      const someNumber = Option.some(1);
      const inner = someNumber.getOr(0);
      expect(inner).toBe(1);
    })

    it('should return the given default value if no some-value is present', () => {
      const nothing: Option<number> = Option.none();
      const inner = nothing.getOr(0);
      expect(inner).toBe(0);
    })

    it('should return the value from the given closure if this is Option.none()', () => {
      const nothing: Option<number> = Option.none();

      const result = nothing.getOr(() => 1);

      expect(result).toBe(1);
    });

    it('should return the inner some-value if this is Option.some()', () => {
      const someNumer = Option.some(1);

      const result = someNumer.getOr(() => 2);

      expect(result).toBe(1);
    });
  })
});
