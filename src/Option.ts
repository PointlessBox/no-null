type Kind = "Some" | "None";

export type None = Option<never>;
export type Some<T> = Option<T>;

/**
 * Option type to avoid null values. Inspired by the rust option enum.
 * Can take two forms: some-option and none-option.
 * some-option represents non-null. none-option represents null
 */
export class Option<T> {
  private constructor(
    private readonly kind: Kind,
    private readonly val?: T,
  ) {
    this.kind = kind
    this.val = val;
    Object.freeze(this);
  }

  /**
   * Indicates if this is a some-option
   */
  get isSome(): boolean {
    return this.kind === "Some";
  }

  /**
   * Indicates if this is a none-option
   */
  get isNone(): boolean {
    return this.kind === "None";
  }

  /**
   * Pattern-matches the inner value. 'onSome' is executed when the inner value is defined and 'onNone' otherwise.
   * If both given closures return a value of the same type, then a value of this type is returned by the exectued branch.
   * @param branches closures which are used to operate on the different branches the Option can take
   * @returns the value returned by the executed branch
   */
  match<R>({ onSome, onNone }: { onSome: (some: T) => R, onNone: () => R }): R {
    if (this.isSome) return onSome(this.val!);
    else return onNone();
  }

  /**
   * Executes the given closure when this is some-option
   * @param call invoked when this is some-option
   */
  ifSome<R>(call: (some: T) => R): Option<R> {
    if (this.isSome) return Option.some(call(this.val!));
    else return Option.none();
  }

  /**
   * Executes the given closure when this is none-option
   * @param call invoked when this is none-option
   */
  ifNone<R>(call: () => R): Option<R> {
    if (this.isNone) return Option.some(call());
    else return Option.none();
  }

  /**
   * Gets the inner some-value if present. Returns the fallback value otherwise
   * @param fallback value to return if this is an none-option
   * @returns inner some-value or fallback-value
   */
  getOr(fallback: T | (() => T)): T {
    function isFunction(func: T | (() => T)): func is () => T {
      return typeof func === 'function';
    }
    return this.match({
      onSome: (some) => some,
      onNone: () => isFunction(fallback)
        ? fallback()
        : fallback,
    })
  }

  /**
   * Factory method to produce a some-option with the given value
   * @param val value contained by the produced some-option
   * @returns Option<T> with the given value inside
   */
  static some<T>(val: T): Some<T> {
    return new Option("Some", val);
  }

  private static readonly _none = new Option<never>("None");
  /**
   * Factory method to produce a none-option, which represents an undefined value
   * @returns a none-option
   */
  static none(): None {
    return Option._none;
  }

  /**
   * Factory method to produce an Option from a nullable value
   * @returns an Option
   */
  static from<T>(val: T | null | undefined): Option<T> {
    return val === null || val == undefined ? Option.none() : Option.some(val);
  }
}
