import { Result } from './Result';
import { Option } from './Option';

const errorMsg = 'e';
const error = new Error(errorMsg);

describe('Result', () => {
  it('should be immutable', () => {
    const okNumber = Result.ok(1);
    const err = Result.err(error);
    expect(() => { (okNumber as any).val = 2; }).toThrow();
    expect(() => { (err as any).val = 2; }).toThrow();
  });

  describe('ok()', () => {
    it('should produce a Result with a value inside', () => {
      const okNumber = Result.ok(1);
      expect((okNumber as any).val).toBe(1);
      expect((okNumber as any).err).toBeUndefined();
    });
  });

  describe('err()', () => {
    it('should produce a Result with an error inside', () => {
      const err = Result.err(error);
      expect((err as any).val).toBeUndefined();
      expect((err as any).err).toBe(error);
    });
  });

  describe('match()', () => {
    it('should execute the onOk function when created with Result.ok()', () => {
      const okNumber = Result.ok(1);
      let didOnOk = false;
      let didOnErr = false;
      okNumber.match({
        onOk: () => { didOnOk = true; },
        onErr: () => { didOnErr = true; }
      });
      expect(didOnOk).toBe(true);
      expect(didOnErr).toBe(false);
    });

    it('should execute the onErr function when created with Result.err()', () => {
      const nothing = Result.err(error);
      let didOnOk = false;
      let didOnErr = false;
      nothing.match({
        onOk: () => { didOnOk = true; },
        onErr: () => { didOnErr = true; }
      });
      expect(didOnOk).toBe(false);
      expect(didOnErr).toBe(true);
    });

    it('should return a value when onOk and onErr both return values', () => {
      const okNumber = Result.ok(1);
      const onOk = () => 1;
      const onErr = () => 0;

      const onOkResult = okNumber.match({ onOk, onErr });

      const err = Result.err(error);
      const onErrResult = err.match({ onOk, onErr });

      expect(onOkResult).toBe(1);
      expect(onErrResult).toBe(0);
    });

    describe('{ onOk }', () => {
      it('should receive the inner value', () => {
        const okNumber = Result.ok(1);
        let innerValue = 0;
        okNumber.match({
          onOk: (ok) => { innerValue = ok; },
          onErr: () => { /* no-op */ }
        });
        expect(innerValue).toBe(1);
      });
    });

    describe('{ onErr }', () => {
      it('should receive the inner error', () => {
        const err = Result.err(error);
        let innerError: Error | null = null;
        err.match({
          onOk: () => { /* no-op */ },
          onErr: (err) => { innerError = err; }
        });
        expect(innerError).toBe(error);
      });
    });
  });

  describe('ifOk()', () => {
    it('should run the given function only when the object was created with Result.ok()', () => {
      const okNumber = Result.ok(1);
      let wasExecuted = false;

      okNumber.ifOk(() => {
        wasExecuted = true;
      });

      expect(wasExecuted).toBe(true);
    });

    it('should not run the given function when the object was Result.err()', () => {
      const err = Result.err(error);
      let wasExecuted = false;

      err.ifOk(() => {
        wasExecuted = true;
      });

      expect(wasExecuted).toBe(false);
    });

    it('should return an Option<R> when called', () => {
      const okNumber = Result.ok(1);

      const numberOption = okNumber.ifOk(() => {
        return 2;
      });

      expect((numberOption as any).val).toBe(2);
    });
  });

  describe('ifErr()', () => {
    it('should run the given function only when the object was created with Result.err()', () => {
      const err = Result.err(error);
      let wasExecuted = false;

      err.ifErr(() => {
        wasExecuted = true;
      });

      expect(wasExecuted).toBe(true);
    });

    it('should not run the given function when the object was Result.ok()', () => {
      const okNumber = Result.ok(1);
      let wasExecuted = false;

      okNumber.ifErr(() => {
        wasExecuted = true;
      });

      expect(wasExecuted).toBe(false);
    });

    it('should return an Option<R> when called', () => {
      const err = Result.err(error);

      const numberOption = err.ifErr(() => {
        return 2;
      });

      expect((numberOption as any).val).toBe(2);
    })
  });

  describe('getOr()', () => {
    it('should return the ok-value if one is present', () => {
      const okNumber = Result.ok(1);
      const inner = okNumber.getOr(0);
      expect(inner).toBe(1);
    });

    it('should return the given default value if no ok-value is present', () => {
      const err = Result.err(error);
      const inner = err.getOr(0);
      expect(inner).toBe(0);
    });

    it('should receive the inner error if one is present', () => {
      const err: Result<boolean, Error> = Result.err(error);
      const gotError = err.getOr((err) => err !== undefined);
      expect(gotError).toBe(true);
    });

    it('should return the value from the given closure if no ok-value is present', () => {
      const err: Result<number, Error> = Result.err(error);
      const inner = err.getOr(() => 0);
      expect(inner).toBe(0);
    });

    it('should return the inner some-value if this is Option.some()', () => {
      const okNumber = Result.ok(1);
      const inner = okNumber.getOr(() => 0);
      expect(inner).toBe(1);
    });
  })
});
